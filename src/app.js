const app = require('express')();
const fs = require('fs');
const hls = require('hls-server');

const path = require('path');
const PORT = 3000;

app.get('/', (req, res) => {
    const options = {
        root: path.join(__dirname)
    };

    return res.status(200).sendFile('client.html', options);
});

const server = app.listen(PORT);

new hls(server, {
    provider: {
        exists: (req, cb) => {
            const ext = req.url.split('.').pop();

            if (ext !== 'm3u8' && ext !== 'ts') {
                return cb(null, true);
            }

            fs.access(__dirname + req.url, fs.constants.F_OK, function (err) {
                if (err) {
                    console.log('File does not exist');
                    return cb(null, false);
                }
                cb(null, true);
            });
        },
        getManifestStream: (req, cb) => {
            const stream = fs.createReadStream(__dirname + req.url);
            cb(null, stream);
        },
        getSegmentStream: (req, cb) => {
            const stream = fs.createReadStream(__dirname + req.url);
            cb(null, stream);
        }
    }
});

