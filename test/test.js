// test-sss.js

var assert = require('chai').assert;

describe("Example tests", function() {
    describe("#first-test", function() {
        it("succeeds always", function() {
            assert(1 == 1, "succeeded as expected");
        });
        it("fails always", function() {
            assert(1 == 0, "fails as expected");
        });
    });
});
